package com.atlassian.stash.repository.hook.ref;


import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.repository.Ref;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RepositoryMetadataService;

import java.util.Collection;

public class ProtectRefHook implements PreReceiveRepositoryHook {

    private final RepositoryMetadataService repositoryMetadataService;

    public ProtectRefHook(RepositoryMetadataService repositoryMetadataService) {
        this.repositoryMetadataService = repositoryMetadataService;
    }


    /**
     * Disables changes to a ref
     */
    @Override
    public boolean onReceive(RepositoryHookContext context, Collection<RefChange> refChanges, HookResponse hookResponse) {
        String refId = context.getSettings().getString("ref-id");
        Ref found = repositoryMetadataService.resolveRef(context.getRepository(), refId);
        for (RefChange refChange : refChanges) {
            if (refChange.getRefId().equals(found.getId())) {
                hookResponse.err().println("The ref '" + refChange.getRefId() + "' cannot be altered.");
                return false;
            }
        }
        return true;
    }
}
