package com.atlassian.stash.repository.hook.ref;

import com.atlassian.stash.repository.Ref;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryMetadataService;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;

public class RefValidator implements RepositorySettingsValidator {

    private final RepositoryMetadataService repositoryMetadataService;

    public RefValidator(RepositoryMetadataService repositoryMetadataService) {
        this.repositoryMetadataService = repositoryMetadataService;
    }

    @Override
    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors settingsValidationErrors, @Nonnull Repository repository) {
        String refId = settings.getString("ref-id", null);
        if (StringUtils.isEmpty(refId)) {
            settingsValidationErrors.addFieldError("ref-id", "The ref id must be specified");
        }else {
            Ref ref = repositoryMetadataService.resolveRef(repository, refId);
            if (ref == null) {
                settingsValidationErrors.addFieldError("ref-id", "Failed to find the ref");
            }
        }
    }
}